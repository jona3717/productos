#!/usr/bin/env python

__author__ = "Jona3717"

import os
import sys
from productos import Productos


class Sistema:
	def __init__(self):
		self.productos = []
		self.menu()
	
	def registrar(self):
		os.system("clear")
		prod = Productos()
	
		codigo = input(str("Código: "))
		nombre = input(str("Nombre del producto: "))
		marca = input(str("Marca del producto: "))
		presentacion = input(str("Presentación: "))
	
		if codigo and nombre and marca and presentacion:
			prod.codigo = codigo
			prod.nombre = nombre
			prod.marca = marca
			prod.presentacion = presentacion
	
			self.productos.append(prod)
			os.system("clear")
			print("Producto registrado con éxito\n")
			self.menu()
		else:
			os.system("clear")
			print("Debes ingresar todos los datos.\n")
			self.menu()
	
	def editar(self, codigo):
		prod = Productos()
		producto = None
	
		for i in self.productos:
			if i.codigo == codigo:
				producto = i
				break
		print("Ingresa solo los datos que deseas modificar,")
		print("de lo contrario pulsa enter.\n")
		codigo = input(str("Código: "))
		nombre = input(str("Nombre del producto: "))
		marca = input(str("Marca del producto: "))
		presentacion = input(str("Presentación: "))
	
		if codigo:
			producto.codigo = codigo
		if nombre:
			producto.nombre = nombre
		if marca:
			producto.marca = marca
		if presentacion:
			producto.presentacion = presentacion
	
		print("¡Realizado!\n")
		self.menu()
	
	def buscar(self, codigo):
		os.system("clear")
		prod = Productos()
		producto = Productos
	
		for i in self.productos:
			if i.codigo == codigo:
				producto = i
				break
			else:
				producto = None
		if producto != None:
			print("Producto:")
			print("---------\n")
			print("Codigo: {}".format(producto.codigo))
			print("Nombre: {}".format(producto.nombre))
			print("Marca: {}".format(producto.marca))
			print("Presentación: {}\n".format(producto.presentacion))
			self.menu()
		else:
			print("El producto no existe")
			self.menu()

	def listar(self):
		os.system("clear")
		if len(self.productos) > 0:
			for i in self.productos:
				print("\nProducto:")
				print("---------\n")
				print("Codigo: {}".format(i.codigo))
				print("Nombre: {}".format(i.nombre))
				print("Marca: {}".format(i.marca))
				print("Presentación: {}\n".format(i.presentacion))
			self.menu()
		else:
			print("No se han registrado productos.\n")
			self.menu()
	
	def menu(self):
		options = {1:"Registrar", 2:"Editar", 3:"Buscar", 4:"Listar", 5:"Salir"}
	
		for i in options:
			print("{}. {}".format(i, options[i]))
	
		selection = input("\nSelecciona una opción: ")
	
		if selection.isnumeric():
			selection = int(selection)
			if selection < 5:
				if selection == 1:
					self.registrar()
				elif selection == 2:
					codigo = input(str("Ingresa el código de producto: "))
					if codigo.isnumeric:
						self.editar(codigo)
					else:
						print("Debes ingresar el código")
				elif selection == 3:
					codigo = input(str("Ingresa el código de producto: "))
					if codigo.isnumeric:
						self.buscar(codigo)
					else:
						print("Debes ingresar el código")
				elif selection == 4:
					self.listar()
				else:
					print("Opción inválida.")
			elif selection == 5:
				print("Adios.")
				sys.exit()
			else:
				print("La opción seleccionada no es válida\n")
				self.menu()
		else:
			print("Solo puedes ingresar números del 1 al 5\n")
			self.menu()


if __name__ == "__main__":
	Sistema()