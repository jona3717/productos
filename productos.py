#!/usr/bin/env python

__author__ = "Jona3717"


class Productos:
	def __init__(self):
		self._codigo = None
		self._nombre = None
		self._marca = None
		self._presentacion = None

	@property
	def codigo(self):
		return self._codigo
	
	@codigo.setter
	def codigo(self, codigo):
		self._codigo = codigo

	@property
	def nombre(self):
		return self._nombre
	
	@nombre.setter
	def nombre(self, nombre):
		self._nombre = nombre

	@property
	def marca(self):
		return self._marca
	
	@marca.setter
	def marca(self, marca):
		self._marca = marca

	@property
	def presentacion(self):
		return self._presentacion
	
	@presentacion.setter
	def presentacion(self, presentacion):
		self._presentacion = presentacion
