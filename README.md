# Productos

![Licencia MIT](https://img.shields.io/badge/Licencia-MIT-green)

## Using Productos

To use productos, open a terminal and type:

python sistema.py


## Contributing to productos

To contribute to **productos**, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.


## Contributors

Thanks to the following people who have contributed to this project:

* [@jona3717](https://gitlab.com/jona3717)

## Contact

If you want to contact me you can reach me at [jona3717](https://jona3717.gitlab.io).

## License

This project uses the following license: [MIT License](https://choosealicense.com/licenses/mit/).